
<head>
<script>
window.onload = function () {

var options = {
	exportEnabled: true,
	animationEnabled: true,
	title:{
		text: "Turn Over VS Close Price"
	},
	
	axisX: {
		title: "States"
	},
	
	axisY: {
		title: "Turn Over",
		titleFontColor: "#C0504E",
		lineColor: "#C0504E",
		labelFontColor: "#C0504E",
		tickColor: "#C0504E",
		includeZero: false
	},
	axisY2: {
		title: "Close Price",
		titleFontColor: "#C0604E",
		lineColor: "#C0604E",
		labelFontColor: "#C0604E",
		tickColor: "#C0604E",
		includeZero: false
	},
	toolTip: {
		shared: true
	},
	legend: {
		cursor: "pointer",
		itemclick: toggleDataSeries
	},
	data: [
	{
		type: "spline",
		name: "Close Price",
		axisYType: "secondary",
		showInLegend: true,
		xValueFormatString: "MMM YYYY",
		yValueFormatString: "$#,##0.#",
		var date = new Date(stock.Date);
			var close = parseFloat(stock.close);
		dataPoints: [
			
                {x:date, y:close}
			
		]
	},
	{
	
		type: "spline",
		name: "Turn Over",
		showInLegend: true,
		xValueFormatString: "MMM YYYY",
		yValueFormatString: "#,##0 Lacs",
		var date = new Date(stock.Date);
		var TO = parseFloat(stock.TurnOverL);
		dataPoints: [
		    {x: date, y:TO}
			
		]		
	}]
};
$("#chartContainer").CanvasJSChart(options);

function toggleDataSeries(e) {
	if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	e.chart.render();
}

}
</script>
 </head> 
 <body>
    {{{ body }}}
    <div id="chartContainer" style="height: 400px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
 </body>