var express = require('express');
var router = express.Router();
var mongo = require('mongodb');
var assert = require('assert');
var unique = require('array-unique');

var url = 'mongodb://localhost:27017/test';
/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' , condition: true, anyArray:[1,2,3]});
});*/

router.get('/', function(req, res, next) {
  var companyArray = []; 
  mongo.connect(url,function(err, db){
    assert.equal(null,err);
    var cursor = db.collection('CompanyList').find();
    cursor.forEach(function(doc, err){
       assert.equal(null, err);
       if(companyArray.includes(doc)){
          console.log('duplicate');
       }
       else{
        companyArray.push(doc);
       }
       
    },function(){
      db.close();

      res.render('index', {company: companyArray});
    });
  });
});
/*router.get('/getChart', function(req, res, next) {
  var companyArray = []; 
  mongo.connect(url,function(err, db){
    assert.equal(null,err);
    var cursor = db.collection('ShareTest').find();
    cursor.forEach(function(doc, err){
       assert.equal(null, err);
       if(companyArray.includes(doc)){
          console.log('duplicate');
       }
       else{
        companyArray.push(doc);
       }
       
    },function(){
      db.close();

      res.render('/layouts/layout', {company: unique(companyArray)});
    });
  });
});*/

router.get('/getJson', function (req, res) {
    var resultArray = []; 
  mongo.connect(url,function(err, db){
    assert.equal(null,err);
    console.log(req.body.selectpicker);
    var cursor = db.collection('ShareTest').find({'company':req.query['selectpicker']});
    cursor.forEach(function(doc, err){
       assert.equal(null, err);
       resultArray.push(doc);
    },function(){
      db.close();
      res.render('index', {items: resultArray});
    });
  });
});

router.get('/about', function(req, res){
    res.render('about');
});

router.get('/table', function(req, res){
    var tableArray = []; 
    mongo.connect(url,function(err, db){
    assert.equal(null,err);
    console.log(req.body.selectpicker);
    var cursor = db.collection('ShareTest').aggregate([{ "$group": { "_id": "$company","value": { "$max": "$Date" }, "o":{ "$avg": "$Open"}, "l":{ "$avg": "$Last"}, "h":{ "$avg": "$High"}, "lo":{ "$avg": "$Low"}, "TTQ":{ "$avg": "$TotalTradeQuantity"}, "TOL":{ "$avg": "$TurnOverL"}, "c":{"$avg":"$Close"}}}]);
    cursor.forEach(function(doc, err){
       assert.equal(null, err);
       tableArray.push(doc);
    },function(){
      db.close();
      res.render('table', {table: tableArray});
    });
  });
    
});
router.get('/',function(req,res, next){
   var chartsArr = []; 
  mongo.connect(url,function(err, db){
    assert.equal(null,err);
    var cursor = db.collection('ShareTest').find();
    cursor.forEach(function(doc, err){
       assert.equal(null, err);
       chartsArr.push(doc);
    },function(){
      db.close();
      res.send('/public/javascripts/charts.js',{charts: chartsArr});
    });
  });
})
/*router.get('/getData/:comp', function(req, res, next) {
  var resultArray = []; 
  mongo.connect(url,function(err, db){
  	assert.equal(null,err);
    var cursor = db.collection('ShareTest').find({'company':req.params.comp});
    cursor.forEach(function(doc, err){
       assert.equal(null, err);
       resultArray.push(doc);
    },function(){
    	db.close();
    	res.render('index', {items: resultArray});
    });
  });
});*/
module.exports = router;
